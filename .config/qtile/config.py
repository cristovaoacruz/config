# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

mod = "mod4"

keys = [
    # Select windows in current stack pane.
    Key([mod],            "j",      lazy.layout.down()),
    Key([mod],            "k",      lazy.layout.up()),
    Key([mod],            "h",      lazy.layout.left()),
    Key([mod],            "l",      lazy.layout.right()),

    # Shift windows around current stack pane.
    Key([mod, "shift"],   "j",      lazy.layout.shuffle_down()),
    Key([mod, "shift"],   "k",      lazy.layout.shuffle_up()),
    Key([mod, "shift"],   "h",      lazy.layout.shuffle_left()),
    Key([mod, "shift"],   "l",      lazy.layout.shuffle_right()),

    # Control current stack pane size.
    Key([mod, "control"], "j",      lazy.layout.grow_down()),
    Key([mod, "control"], "k",      lazy.layout.grow_up()),
    Key([mod, "control"], "h",      lazy.layout.grow_left()),
    Key([mod, "control"], "l",      lazy.layout.grow_right()),
    Key([mod, "control"], "Return", lazy.layout.toggle_split()),
    Key([mod, "control"], "m",      lazy.layout.maximize()),
    Key([mod, "control"], "n",      lazy.layout.normalize()),

    # Switch window focus to other pane(s) of stack
    #Key([mod],            "space",  lazy.layout.next()),

    # Swap panes of split stack
    #Key( [mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between different layouts as defined below
    Key([mod],            "space",  lazy.next_layout()),
    Key([mod, "shift"],   "space",  lazy.prev_layout()),

    Key([mod], "w", lazy.window.kill()),
    Key([mod], "r", lazy.spawncmd()),

    Key([mod, "control"], "r",      lazy.restart()),
    Key([mod, "control"], "q",      lazy.shutdown()),

    Key([mod],            "Return", lazy.spawn("stmux")),
    Key([mod],            "period", lazy.next_screen()),
    Key([mod],            "comma" , lazy.prev_screen()),

    # Screen brightness control.
    Key([], "XF86MonBrightnessUp",   lazy.spawn("set-screen-brightness +5")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("set-screen-brightness -5")),
    
    # Speaker control.
    Key([],          "XF86AudioRaiseVolume",  lazy.spawn("pamixer -i 5")),
    Key([],          "XF86AudioLowerVolume",  lazy.spawn("pamixer -d 5")),
    Key(["control"], "XF86AudioRaiseVolume",  lazy.spawn("pamixer --allow-boost -i 5")),
    Key(["control"], "XF86AudioLowerVolume",  lazy.spawn("pamixer --allow-boost -d 5")),
    Key([],          "XF86AudioMute",         lazy.spawn("pamixer -t")),
    
    # Microphone control.
    Key(["shift"],            "XF86AudioRaiseVolume", lazy.spawn("pamixer --default-source -i 5")),
    Key(["shift"],            "XF86AudioLowerVolume", lazy.spawn("pamixer --default-source -d 5")),
    Key(["control", "shift"], "XF86AudioRaiseVolume", lazy.spawn("pamixer --default-source --allow-boost -i 5")),
    Key(["control", "shift"], "XF86AudioLowerVolume", lazy.spawn("pamixer --default-source --allow-boost -d 5")),
    Key(["shift"],            "XF86AudioMute",        lazy.spawn("pamixer --default-source -t")),
    Key([],                   "XF86AudioMicMute",     lazy.spawn("pamixer --default-source -t")),

    # Display control.
    Key([mod], "p", lazy.spawn("config-display")),

    # Show and hide task bar.
    Key([mod], "b", lazy.hide_show_bar("bottom")),
]

groups = [Group(i) for i in "123456789"]

for i in groups:
    # mod1 + letter of group = switch to group
    keys.append(
        Key([mod], i.name, lazy.group[i.name].toscreen())
    )

    # mod1 + shift + letter of group = switch to & move focused window to group
    keys.append(
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name))
    )

layouts = [
    layout.Columns(),
    layout.VerticalTile(),
    layout.Matrix(),
    layout.TreeTab(font='xos4 Terminus'),
    layout.Floating(),
    layout.Max(),
]

widget_defaults = dict(
    font='xos4 Terminus',
    fontsize=14,
    padding=3,
)

screens = [
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(),
                widget.CurrentLayoutIcon(),
                widget.GroupBox(),
                widget.Prompt(),
                #widget.WindowName(),
                widget.TaskList(font='xos4 Terminus'),
                widget.Sep(),
                widget.Systray(),
                widget.Sep(),
                widget.Volume(
                    update_interval     = 1,
                ),
                widget.Sep(),
                widget.Volume(
                    update_interval     = 1,
                    direction           = "source",
                ),
                widget.Sep(),
                #widget.Maildir(),
                #widget.Sep(),
                widget.Backlight(backlight_name="intel_backlight"),
                widget.Sep(),
                widget.Battery(),
                widget.Sep(),
                widget.Wlan(),
                widget.Sep(),
                widget.Clock(format='%Y-%m-%d %H:%M %z (W%W)'),
            ],
            30,
        ),
    ),
    Screen(
        bottom=bar.Bar(
            [
                widget.CurrentScreen(),
                widget.CurrentLayoutIcon(),
                widget.GroupBox(),
                widget.Prompt(),
                #widget.WindowName(),
                widget.TaskList(font='xos4 Terminus'),
            ],
            30,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
        start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
        start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating()
auto_fullscreen = True
focus_on_window_activation = "smart"
extentions = []

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

@hook.subscribe.screen_change
def restart_on_randr(qtile, ev):
    qtile.cmd_restart()

