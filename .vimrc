set autoindent
set backspace=2
syntax on
set spelllang=en_gb
set nospell
colo slate
set guifont=xos4Terminus\ 15
set shiftwidth=8
set tabstop=8
set noexpandtab
set textwidth=80
highlight spellbad gui='none' guifg='white' guibg='red'
highlight spellcap gui='none' guifg='white' guibg='blue'
set spellfile=~/.vim/spell/en.utf-8.add

set sts=0
" Indent C function continuation aligned with parenthesis.
set cinoptions=(0,u0,U0

" vim -b : edit binary using xxd-format!
augroup Binary
  au!
  au BufReadPre  *.bin let &bin=1
  au BufReadPost *.bin if &bin | %!xxd
  au BufReadPost *.bin set ft=xxd | endif
  au BufWritePre *.bin if &bin | %!xxd -r
  au BufWritePre *.bin endif
  au BufWritePost *.bin if &bin | %!xxd
  au BufWritePost *.bin set nomod | endif
augroup END
