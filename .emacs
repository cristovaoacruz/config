; Got this from http://orgmode.org/worg/org-tutorials/orgtutorial_dto.html
(require 'org)
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)

; Got this from http://orgmode.org/manual/Setting-up-capture.html#Setting-up-capture
(define-key global-map "\C-cc" 'org-capture)
(setq org-capture-templates
      '(("e" "Emacs" entry (file "~/org/emacs.org"))
        ("m" "Music" entry (file "~/org/music.org"))))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes (quote (deeper-blue)))
 '(custom-safe-themes
   (quote
    ("dd1cf47034b1c20f5f43cd91ae76f00abef05f91b7be57d94653c493bcf41dda" default)))
 '(doc-view-continuous t)
 '(doc-view-pdf->png-converter-function (quote doc-view-pdf->png-converter-mupdf))
 '(doc-view-resolution 300)
 '(doc-view-scale-internally nil)
 '(fci-rule-color "#f8fce8")
 '(fill-column 78)
 '(hl-paren-background-colors (quote ("#e8fce8" "#c1e7f8" "#f8e8e8")))
 '(hl-paren-colors (quote ("#40883f" "#0287c8" "#b85c57")))
 '(org-capture-templates
   (quote
    (("m" "Music" plain
      (file "~/org/music.org")
      "")
     ("e" "Emacs" entry
      (file "~/org/emacs.org")
      ""))) t)
 '(package-selected-packages (quote (cuda-mode plan9-theme)))
 '(sml/active-background-color "#98ece8")
 '(sml/active-foreground-color "#424242")
 '(sml/inactive-background-color "#4fa8a8")
 '(sml/inactive-foreground-color "#424242")
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'doc-view)
(setq doc-view-resolution 144)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(menu-bar-mode -1)
(scroll-bar-mode -1)

(setq org-agenda-files '("~/depot/ni/org/log"))

(add-to-list 'default-frame-alist
	     '(font . "xos4Terminus:size=18"))

(load "/usr/share/clang/clang-format.el")
(global-set-key [C-M-tab] 'clang-format-region)
