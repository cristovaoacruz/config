# # If not running interactively, don't do anything
# [[ $- != *i* ]] && return

export VISUAL=vim
export EDITOR=vim
export SHELL=bash
export TZ='Europe/Helsinki'

export PATH="/home/cac/scripts/:/home/cac/.local/bin:$PATH"
export MANPATH="/home/cac/.local/share/man:$MANPATH"

export MAKEPKG_CONF=~/.local/etc/makepkg.conf

#source ~/env/texlive-2016
#source ~/env/ruby-gem
#source ~/env/matlab-2016b
